package com.mars.security.browser;

import com.mars.security.core.properties.SecurityProperties;
import com.mars.security.core.support.SimpleResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * controller请求:处理身份认证请求:
 * 1.RequestCache
 * 2.RedirectStrategy
 * @author MARS
 * @date 2018/7/20
 */
@RestController
public class BrowserSecurityController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    // 请求缓存
    private RequestCache requestCache = new HttpSessionRequestCache();

    // 请求跳转
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();


    @Autowired
    private SecurityProperties securityProperties;

    //@Autowired
    //private ProviderSignInUtils providerSignInUtils;

    /**
     * 需要身份认证时跳转到这里
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/authentication/require")
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED) //响应状态码
    public SimpleResponse requireAuthentication(HttpServletRequest request, HttpServletResponse response)throws IOException{
        // 获取引发跳转的请求
        SavedRequest savedRequest = requestCache.getRequest(request,response);
        if(savedRequest != null) {
            String target = savedRequest.getRedirectUrl();
            logger.info("引发跳转的请求是：" + target);
            if (StringUtils.endsWithIgnoreCase(target,".html")) {
                // 跳转
                redirectStrategy.sendRedirect(request,response,securityProperties.getBrowser().getLoginPage());
            }
        }
        return new SimpleResponse("访问的服务需要身份认证，请引导用户前往登陆页");
    }

    /**
     * 返回socialUserInfo
     */
    //@GetMapping("/social/user")
    //public SocialUserInfo getSocialUserInfo(HttpServletRequest request){
    //    SocialUserInfo socialUserInfo = new SocialUserInfo();
    //    Connection<?> connection = providerSignInUtils.getConnectionFromSession(new ServletWebRequest(request));
    //    socialUserInfo.setProviderId(connection.getKey().getProviderId());
    //    socialUserInfo.setProviderUserId(connection.getKey().getProviderUserId());
    //    socialUserInfo.setNickname(connection.getDisplayName());
    //    socialUserInfo.setHeadimg(connection.getImageUrl());
    //    return socialUserInfo;
    //}
}
