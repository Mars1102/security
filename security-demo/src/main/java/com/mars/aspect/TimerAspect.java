package com.mars.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * 切片（类）:  用注解的形式: @Aspect
 * 1、切入点：
 *  -- 在哪些方法上起作用
 *  -- 在什么时候起作用
 * 2、增强(起作用时，编写逻辑处理代码)
 * @author MARS
 * @date 2018/7/18
 */
//@Aspect
//@Component
public class TimerAspect {

    /**
     * 声明切入点;
     * 1.哪些方法上起作用:用到aop表达式
     * 2.什么时候起作用：有四个注解 @Before/@After/@AfterThrowing/@Around
     * 3.ProceedingJoinPoint 表示当前拦截的方法的具体信息对象，类似于interceptor中的HandlerMethod
     */

    @Around("execution(* com.mars.controller.UserController.*(..))")
    public Object handlerControllerMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("time aspect start");
        // 获取请求的参数
        Object[] args = proceedingJoinPoint.getArgs();

        for (Object obj : args) {
            System.out.println("arg is :"+ obj);
        }

        Long start = System.currentTimeMillis();
        // 表示被控制器拦截住的方法
        Object object = proceedingJoinPoint.proceed();

        System.out.println("time aspect 耗时："+(System.currentTimeMillis()-start));
        System.out.println("time aspect end");

        return object;
    }

}
