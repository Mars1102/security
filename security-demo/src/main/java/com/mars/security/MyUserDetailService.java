package com.mars.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.security.SocialUser;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;

/**
 * 可以连接数据库，获取用户信息，进行认证
 *
 * 第三方登陆需要实现 SocialUserDetailsService 接口
 *
 * @author MARS
 * @date 2018/7/19
 */

@Component
public class MyUserDetailService implements UserDetailsService,SocialUserDetailsService{

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 可以注入与连接数据库操作的dao或者是repository（spring jpa）或者是mapper
     * @Autowired
     * private UserMapper usermapper ,查询用户
     * PasswordEncoder  处理用户密码加密解密的类
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Autowired PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("表单用户登陆用户信息：" + username);
        // <1>根据用户名查找用户信息
        //   User 使用的是security提供的user，已经实现UserDetail接口
        //   User(username,password,authorizes)
        //     -- 参数1表示：用户登录名称
        //     -- 参数2表示：从数据库中根据用户登录名称查询出来的密码(实际开发中，从数据库读出)
        //     -- 参数3表示：该用户的权限集合(实际开发中，从数据库读出)
        // return new User(username,"123456", AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
        // <2>根据查找到的用户信息判断用户是否被冻结
        // <3>处理密码加密解密 PasswordEncoder接口

        //String dbpassword = passwordEncoder.encode("123456");
        //logger.info("从数据库获取的加密码：" + dbpassword);
        //return new User(username,dbpassword,
        //        true, true,  true, true,
        //        AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
        return buildUser(username);
    }

    /**
     * 社交登陆
     *
     * @param userId
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        logger.info("社交用户登陆用户信息：" + userId);
        String dbpassword = passwordEncoder.encode("123456");
        logger.info("从数据库获取的加密码：" + dbpassword);
        //return new SocialUser(userId,dbpassword,
        //        true, true,  true, true,
        //        AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
        return buildUser(userId);
    }

    private SocialUserDetails buildUser(String userId){
        String dbpassword = passwordEncoder.encode("123456");
        return new SocialUser(userId,dbpassword,
                true, true,  true, true,
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }
}
