package com.mars.controller;

import com.mars.exception.UserNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 1.@ControllerAdvice ：去处理全局异常的处理器
 * 2.在方法上添加，处理什么异常类型  @ExceptionHandler(UserNotExistException.class)
 * 3.@ResponseBody：异常响应为json串
 * 4.@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)：异常响应的状态码
 * @author MARS
 * @date 2018/7/17
 */

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(UserNotExistException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String,Object> handlerUserNotExistExceptino(UserNotExistException ex) {
        // 获取异常信息
        Map<String,Object> map = new HashMap<>();
        map.put("id",ex.getId());
        map.put("message",ex.getMessage());
        return map;
    }
}

