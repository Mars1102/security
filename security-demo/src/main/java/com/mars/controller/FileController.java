package com.mars.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;

import com.mars.dto.FileInfo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * 文件上传和下载
 * @author MARS
 * @date 2018/7/18
 */
@RestController
@RequestMapping("/file")
public class FileController {

    private String folder = "E:\\WorkSpace\\springboot_workspace\\security\\security-demo\\src\\main\\java\\com\\mars\\controller\\";

    /**
     *
     * @param file：要与前端的file名称保持一致
     * @return
     */
    @PostMapping
    public FileInfo upload(MultipartFile file) throws Exception {
        // 前端传入的参数的字段
        System.out.println(file.getName());
        //  原始文件名
        System.out.println(file.getOriginalFilename());
        // 文件尺寸
        System.out.println(file.getSize());

        File localFile = new File(folder + System.currentTimeMillis() + ".txt");
        // 写入本地文件，如果使用oss或者是文件服务器，就使用file.getInputStream() 把字节流读取出来，写入任何地方
        file.transferTo(localFile);
        return new FileInfo(localFile.getAbsolutePath());
    }


    /**
     * 下载文件，是get请求
     *
     */
    @GetMapping("/{id}")
    public void download(HttpServletRequest request, HttpServletResponse response,@PathVariable String id) throws Exception{

        /**
         * jdk1.7新语法
         * 流声明在try后的()内，在方法执行完毕后，会自动关闭流，不用写finally语句块关闭流
         */

        try (
                InputStream inputStream = new FileInputStream(new File(folder,id+".txt"));
                OutputStream outputStream = response.getOutputStream();
                ) {
            // 设置下载的文件头
            response.setContentType("applicatoin/x-download");
            // 设置下载的文件名
            response.addHeader("Content-Disposition","attachment;filename=test.txt");
            // 把文件的输入流 copy 到输出流中
            IOUtils.copy(inputStream,outputStream);
            outputStream.flush();
        }
    }



}
