package com.mars.security.core.social.qq.connect;

import com.mars.security.core.social.qq.api.QQ;
import com.mars.security.core.social.qq.api.QQUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * 实现 ApiAdapter 适配类,泛型参数类型只的是当前api实现的类型
 * @Author: Mars
 * @Date: 2018/8/31 11:39
 */
public class QQAdapter implements ApiAdapter<QQ>{

    /**
     * 测试当前API是否可用
     * @param api
     * @return
     */
    @Override
    public boolean test(QQ api) {
        return true;
    }

    /**
     * 创建适配项
     * @param api
     * @param values
     */
    @Override
    public void setConnectionValues(QQ api, ConnectionValues values) {
        // 获取用户信息
        QQUserInfo userInfo = api.getUserInfo();
        //把用户信息的值设置到ConnectionValues中
        // 设置显示用户的名字
        values.setDisplayName(userInfo.getNickname());
        // 获取用户头像
        values.setImageUrl(userInfo.getFigureurl_1());
        // 获取个人主页url
        //values.setProfileUrl();
        // 获取服务商的用户id，也就是openid
        values.setProviderUserId(userInfo.getOpenId());
    }

    /**
     * 绑定解绑
     * @param api
     * @return
     */
    @Override
    public UserProfile fetchUserProfile(QQ api) {
        return null;
    }

    /**
     *
     * @param api
     * @param message
     */
    @Override
    public void updateStatus(QQ api, String message) {

    }
}
