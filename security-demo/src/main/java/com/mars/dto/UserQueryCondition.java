package com.mars.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用户查询条件对象
 * @author MARS
 * @date 2018/7/15
 */
public class UserQueryCondition {

    @ApiModelProperty("用户名称")
    private String username;
    @ApiModelProperty("用户年龄起始值")
    private int age;
    @ApiModelProperty("用户年龄终止值")
    private int ageTo;
    @ApiModelProperty("其他信息")
    private String xxx;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(int ageTo) {
        this.ageTo = ageTo;
    }

    public String getXxx() {
        return xxx;
    }

    public void setXxx(String xxx) {
        this.xxx = xxx;
    }
}
