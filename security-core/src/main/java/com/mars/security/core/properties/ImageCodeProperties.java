package com.mars.security.core.properties;

import com.mars.security.core.validate.code.sms.SmsCodeSender;

/**
 * 重构图形验证码，支持验证码图片大小的参数化
 *
 * @author MARS
 * @date 2018/7/27
 */
public class ImageCodeProperties extends SmsCodeProperties{

    private int width = 67;
    private int height = 23;

    // 设置图片验证码的长度为固定的 4
    public ImageCodeProperties() {
        setLength(4);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
