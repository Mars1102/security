package com.mars.security.core.validate.code;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * 短信验证码：
 *
 * @author MARS
 * @date 2018/7/25
 */
public class ValidateCode {

    /**
     * 验证码
     *
     */
    private String code;

    /**
     * 过期时间
     *
     */
    private LocalDateTime expireTime;

    /**
     * 过期时间点
     * @param code
     * @param expireIn
     */
    public ValidateCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    public ValidateCode(String code, LocalDateTime expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    // 设置过期时间
    public boolean isExpired(){
        return LocalDateTime.now().isAfter(expireTime);
    }
}
