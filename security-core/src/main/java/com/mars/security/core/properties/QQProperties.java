package com.mars.security.core.properties;

import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * 创建社交 properties :
 * 1.继承 SocialProperties
 *
 * @author MARS
 * @date 2018/7/22
 */
public class QQProperties extends SocialProperties {

    // 供应商标识
    private String providerId = "qq";

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
