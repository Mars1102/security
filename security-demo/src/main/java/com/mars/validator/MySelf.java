package com.mars.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.PARAMETER;

/**
 * 自定义注解：
 * 1、使用@interface定义名称
 * 2、添加@Target(ElementType.TYPE):表示在哪些地方可以使用
 * 3、添加@Retention(RetentionPolicy.RUNTIME) 运行时的注解
 * 4、添加@Constraint(validatedBy = XXx.class) 表示使用该注解进行校验的逻辑代码类
 * 5、注解内不能为空内容
 * @author MARS
 * @date 2018/7/17
 */

@Target({ METHOD, FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MySelValidator.class)
public @interface MySelf {
    String message() default "{org.hibernate.validator.constraints.NotBlank.message}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
