package com.mars.security.core.properties;

/**
 * 验证码配置集合：包含图形验证码 和 短信验证码
 * @author MARS
 * @date 2018/7/27
 */
public class ValidateCodeProperties {

    /**
     * 引入 ImageCodeProperties
     *
     */
    private ImageCodeProperties image = new ImageCodeProperties();

    public ImageCodeProperties getImage() {
        return image;
    }

    public void setImage(ImageCodeProperties image) {
        this.image = image;
    }

    /**
     * 引入短信配置
     */
    private SmsCodeProperties sms = new SmsCodeProperties();

    public SmsCodeProperties getSms() {
        return sms;
    }

    public void setSms(SmsCodeProperties sms) {
        this.sms = sms;
    }
}
