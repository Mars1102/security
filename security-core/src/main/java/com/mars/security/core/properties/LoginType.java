package com.mars.security.core.properties;

/**
 * 登录枚举类型
 * @author MARS
 * @date 2018/7/23
 */
public enum LoginType {
    REDIRECT,
    JSON
}
