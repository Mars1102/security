package com.mars.security;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Component;

/**
 * 自定义第三方用户匿名登录，实现 ConnectionSignUp 接口
 * @Author: Mars
 * @Date: 2018/9/20 11:21
 */
@Component
public class DemoConnectionSignUp implements ConnectionSignUp {

    @Override
    public String execute(Connection<?> connection) {
        // 根据社交用户信息，默认创建用户，返回用户唯一标识
        return connection.getDisplayName();
    }
}
