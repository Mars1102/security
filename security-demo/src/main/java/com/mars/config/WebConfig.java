package com.mars.config;

import com.mars.filter.TimerFilter;
import com.mars.interceptor.TimerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 1、创建一个类，添加@Configuration，表示该类为配置类
 * 2、使用@Bean表示该类被注册,FilterRegistrationBean
 * 3、继承 WebMvcConfigurerAdapter ，可以调用其interceptor的方法，将定义的interceptor加载到配置文件中
 * @author MARS
 * @date 2018/7/17
 */

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter{

    @Autowired
    //private TimerInterceptor timerInterceptor;

    /**
     * 注册filter，相当于web.xml中配置<filter></filter>
     * @return
     */
    //@Bean
    public FilterRegistrationBean timeFilter(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        // 引入自定义的filter
        TimerFilter timerFilter = new TimerFilter();
        // 将自定义的filter注册到提供的FilterRegistrationBean中
        registrationBean.setFilter(timerFilter);

        // 设置拦截的路径
        List<String> urls = new ArrayList<>();
        urls.add("/*");
        registrationBean.setUrlPatterns(urls);

        return registrationBean;
    }

    /**
     * 添加拦截器配置：
     * 1、继承 WebMvcConfigurerAdapter 接口，重写 addInterceptors()
     * @param registry：interceptor注册器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //registry.addInterceptor(timerInterceptor);
    }

    /**
     * 拦截异步处理的请求，异步处理的配置
     * @param configurer
     */
    //@Override
    //public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
    //    configurer.registerCallableInterceptors();
    //    configurer.registerDeferredResultInterceptors();
    //    super.configureAsyncSupport(configurer);
    //}
}
