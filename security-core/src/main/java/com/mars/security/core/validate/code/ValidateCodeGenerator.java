package com.mars.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 重构校验码生成逻辑：
 * 校验码的验证生成器接口
 *
 * @author MARS
 * @date 2018/7/29
 */
public interface ValidateCodeGenerator {

    ValidateCode generate(ServletWebRequest request);
}
