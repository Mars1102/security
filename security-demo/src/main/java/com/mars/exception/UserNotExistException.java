package com.mars.exception;

/**
 * 自定义异常：
 * 1、编写构造函数
 * 2、可自定义属性，一起抛出
 * 3、创建异常处理器 ExceptionHandler
 * @author MARS
 * @date 2018/7/17
 */
public class UserNotExistException extends RuntimeException {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserNotExistException(String id) {
        super("用户不存在");
        this.id = id;
    }
}
