package com.mars.validator;

import com.mars.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 注解字段逻辑处理类，需要实现ConstraintValidator接口
 * ConstraintValidator<A,T> :
 * -- A :表示验证的注解是什么
 * -- T ：表示验证的字段的类型是什么
 *
 * 该校验类中可以添加 @autowired注解类
 * @author MARS
 * @date 2018/7/17
 */
public class MySelValidator implements ConstraintValidator<MySelf,Object>{

    @Autowired
    private HelloService helloService;

    /**
     * 校验器初始化做的工作
     * @param constraintAnnotation
     */
    @Override
    public void initialize(MySelf constraintAnnotation) {
        System.out.println("myself init");
    }

    /**
     * 真正的校验逻辑方法
     * @param value ： 注解添加在某一字段上，该字段传入的值
     * @param context ： 上下文
     * @return
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        helloService.greeting("tom");
        System.out.println(value);
        return false;
    }
}


