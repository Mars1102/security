package com.mars.security.core.properties;

/**
 * @author MARS
 * @date 2018/7/22
 */
public class BrowserProperties {

    // 默认注册页提示
    private String signUpUrl = "/imooc-signUp.html";

    private String loginPage = "/login.html";

    private LoginType loginType = LoginType.JSON;

    // 配置remember-me的过期时间
    private int rememberMeSeconds = 3600;

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public int getRememberMeSeconds() {
        return rememberMeSeconds;
    }

    public void setRememberMeSeconds(int rememberMeSeconds) {
        this.rememberMeSeconds = rememberMeSeconds;
    }

    public String getSignUpUrl() {
        return signUpUrl;
    }

    public void setSignUpUrl(String signUpUrl) {
        this.signUpUrl = signUpUrl;
    }
}
