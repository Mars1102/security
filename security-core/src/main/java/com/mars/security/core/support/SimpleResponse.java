package com.mars.security.core.support;

/**
 * 封装转换响应对象
 * @author MARS
 * @date 2018/7/22
 */
public class SimpleResponse {

    private Object content;

    public SimpleResponse(Object content) {
        this.content = content;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
