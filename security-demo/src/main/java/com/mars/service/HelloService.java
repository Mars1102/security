package com.mars.service;

/**
 * @author MARS
 * @date 2018/7/17
 */
public interface HelloService {

    String greeting(String name);
}
