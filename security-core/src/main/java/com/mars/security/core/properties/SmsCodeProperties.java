package com.mars.security.core.properties;

/**
 * 短信验证码
 *
 * @author MARS
 * @date 2018/7/27
 */
public class SmsCodeProperties {

    private int length = 6;
    // 过期时间60秒
    private int expireIn = 60 ;

    // 配置多个url处理
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(int expireIn) {
        this.expireIn = expireIn;
    }
}
