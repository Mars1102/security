package com.mars.security.browser.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mars.security.core.properties.LoginType;
import com.mars.security.core.properties.SecurityProperties;
import com.mars.security.core.support.SimpleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义登陆发生错误后的结果返回:
 * 1.实现 AuthenticationFailureHandler接口
 * 2、重写 onAuthenticationFailure()方法
 * 3、重构时，继承 SimpleUrlAuthenticationFailureHandler 类
 * @author MARS
 * @date 2018/7/23
 */

@Component("myAuthenticationFailureHandler")
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler{


    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SecurityProperties securityProperties;

    /**
     *
     * @param request
     * @param response
     * @param exception  错误异常信息
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response
            , AuthenticationException exception) throws IOException, ServletException {
        logger.info("登录失败");
        if (LoginType.JSON.equals(securityProperties.getBrowser().getLoginType())) {
            // 返回失败的状态码,自定义如果是json就走这里，否则执行默认的失败响应方法
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(new SimpleResponse(exception.getMessage())));
        } else {
            super.onAuthenticationFailure(request,response,exception);
        }
    }

    public static void main(String[] args) {
        // 请求路径匹配处理
        AntPathMatcher matcher = new AntPathMatcher();
        String pattern = "/user/**";
        String url = "/user/query/1";
        boolean match = matcher.match(pattern, url);
        System.out.println(match);
    }
}

