package com.mars.security.core.social.qq.config;

import com.mars.security.core.properties.QQProperties;
import com.mars.security.core.properties.SecurityProperties;
import com.mars.security.core.social.qq.connect.QQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.social.SocialAutoConfigurerAdapter;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.ConnectionFactory;

/**
 * @Author: Mars
 * @Date: 2018/8/31 15:00
 */

@Configuration
@ConditionalOnProperty(prefix = "mars.security.social.qq",name = "app-id")
public class QQAutoConfig extends SocialAutoConfigurerAdapter {
    @Autowired
    private SecurityProperties securityProperties;

    /**
     * 创建连接工厂
     * @return
     */
    @Override
    protected ConnectionFactory<?> createConnectionFactory() {
        QQProperties qqConfig = securityProperties.getSocial().getQq();
        return new QQConnectionFactory(qqConfig.getProviderId(),
                qqConfig.getAppId(),qqConfig.getAppSecret());
    }
}
