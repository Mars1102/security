package com.mars.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author masisi
 * @date 2023/1/31 17:40
 * @Description 校验码存储器
 */
public interface ValidateCodeRepository {


    /**
     * 保存验证码
     * @param request
     * @param code
     * @param validateCodeType
     */
    void save(ServletWebRequest request, ValidateCode code, ValidateCodeType validateCodeType);
    /**
     * 获取验证码
     * @param request
     * @param validateCodeType
     * @return
     */
    ValidateCode get(ServletWebRequest request, ValidateCodeType validateCodeType);
    /**
     * 移除验证码
     * @param request
     * @param codeType
     */
    void remove(ServletWebRequest request, ValidateCodeType codeType);
}
