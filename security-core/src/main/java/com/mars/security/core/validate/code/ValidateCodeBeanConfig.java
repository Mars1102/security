package com.mars.security.core.validate.code;

import com.mars.security.core.properties.SecurityProperties;
import com.mars.security.core.validate.code.image.ImageCodeGenerator;
import com.mars.security.core.validate.code.sms.DefaultSmsCodeSender;
import com.mars.security.core.validate.code.sms.SmsCodeGenerator;
import com.mars.security.core.validate.code.sms.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 实现接口的动态化，可覆盖默认的实现方法
 *
 * Java编程式注入bean:
 * 1、添加@Bean注解
 * 2、默认方法名为，注入bean的name
 *
 * Spring自动注入@Autowired，遇到map和list的用法：
 * 1、@Autowired
 *    Map<String,ValidateCodeGeneraotr> map ;
 *
 * --解读：只要是实现了 ValidateCodeGeneraotr接口的类，在spring启动时，会自动注入到map中，
 *           key，默认是实现类的名称，
 *           value，是实现类
 *
 *
 * @author MARS
 * @date 2018/7/29
 */

@Configuration
public class ValidateCodeBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    /**
     * @ConditionalOnMissingBean注解意思是当spring容器不存在imageCodeGenerator时才给配置一个该bean
     * 作用是使程序更具可扩展性，该配置类是配置在core模块，这就意味着，如果引用该模块的项目
     * 如果有一个自己的实现，实现了ValidateCodeGenerator接口，定义了自己的实现，名字也叫imageCodeGenerator时，
     * 就用应用级别的实现，没有的话就用这个默认实现
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(name = "imageCodeGenerator")
    public ValidateCodeGenerator imageCodeGenerator(){
        ImageCodeGenerator codeGenerator = new ImageCodeGenerator();
        codeGenerator.setSecurityProperties(securityProperties);
        return codeGenerator;
    }

    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeSender(){
        return new DefaultSmsCodeSender();
    }

}
