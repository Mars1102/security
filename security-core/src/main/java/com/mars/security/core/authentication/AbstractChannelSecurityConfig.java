package com.mars.security.core.authentication;

import com.mars.security.core.properties.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * @author masisi
 * @date 2023/2/1 09:14
 * @Description
 */
public class AbstractChannelSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    protected AuthenticationSuccessHandler imoocAuthenticationSuccessHandler;

    @Autowired
    protected AuthenticationFailureHandler imoocAuthenticationFailureHandler;

    protected void applyPasswordAuthenticationConfig(HttpSecurity http) throws Exception {
        //http.httpBasic()  --> httpBasic 认证登陆,是弹出框的认证
        // 设置表单登陆
        http.formLogin()
                // 个性化登陆页面配置
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
                // 配置自定义处理登录url
                .loginProcessingUrl(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM)
                // 自定义配置成功登录后的处理
                .successHandler(imoocAuthenticationSuccessHandler)
                // 自定义配置登陆失败后的处理
                .failureHandler(imoocAuthenticationFailureHandler);
    }
}
