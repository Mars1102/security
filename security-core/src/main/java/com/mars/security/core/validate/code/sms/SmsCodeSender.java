package com.mars.security.core.validate.code.sms;

/**
 *
 * 公用短信验证码封装
 *
 * @author MARS
 * @date 2018/7/31
 */
public interface SmsCodeSender {

    /**
     *
     * @param mobile 发送手机号
     * @param code 验证码
     */
    void send(String mobile,String code);
}
