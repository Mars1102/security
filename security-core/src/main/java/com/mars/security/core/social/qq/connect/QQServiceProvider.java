package com.mars.security.core.social.qq.connect;

import com.mars.security.core.social.qq.api.QQ;
import com.mars.security.core.social.qq.api.QQImpl;
import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

/**
 * 创建ServiceProvider：
 * 包含第一步到第五步
 * 1、继承 springsecurity 提供的AbstractOAuth2ServiceProvider类，需要定义泛型类型，
 *    泛型类型为api的接口类型
 * @Author: Mars
 * @Date: 2018/8/31 11:26
 */
public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQ>{

    private String appId;

    // 第一步，用户导向仁恒服务器的url
    private static final String URL_AUTHORIZE = "https://graph.qq.com/oauth2.0/authorize";
    // 第四步，三方申请令牌的url
    private static final String URL_ACCESS_TOKEN = "https://graph.qq.com/oauth2.0/token";

    /**
     * 需要实现springsecurity提供的OAuth2Template
     *
     * @param appId
     * @param appSecret
     */
    public QQServiceProvider(String appId,String appSecret) {
        // 替换掉原有的 OAuth2Template 模板，使用自定义的OAuth模板
        super(new QQOAuth2Template(appId,appSecret,URL_AUTHORIZE,URL_ACCESS_TOKEN));
        this.appId = appId;
    }

    /**
     * 返回泛型接口的实现
     * @param accessToken
     * @return
     */
    @Override
    public QQ getApi(String accessToken) {
        return new QQImpl(accessToken,appId);
    }
}
