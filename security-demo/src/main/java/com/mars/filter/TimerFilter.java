package com.mars.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.Date;

/**
 * 拦截的三种方法：
 * 1、过滤器拦截：实现Filter(java.servlet)接口
 * 2、在doFilter里写请求拦截的代码
 * @author MARS
 * @date 2018/7/17
 */
//@Component
public class TimerFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("time filter init");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("time filter start");
        // 开始时间
        long start = System.currentTimeMillis();
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("time filter 耗时："+(System.currentTimeMillis() - start));
        System.out.println("time filter finished");
    }

    @Override
    public void destroy() {
        System.out.println("time filter destroy");
    }
}
