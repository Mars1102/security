package com.mars.security.core.validate.code;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义验证码错误异常：
 * 1、继承 AuthenticationException 类，是SpringSecurity异常的一个基类
 * 2.
 * @author MARS
 * @date 2018/7/26
 */
public class ValidateCodeException extends AuthenticationException{

    public ValidateCodeException(String msg, Throwable t) {
        super(msg, t);
    }

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
