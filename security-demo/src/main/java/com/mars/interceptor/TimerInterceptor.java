package com.mars.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器方式的拦截（可以看到spring控制器的东西）
 * 1、实现HandlerInterceptor方法
 * 2、从HandlerMethod中获取请求信息
 * 3、拦截器生效，需要添加@component注解注册，同时还需要在类似于web.xml的配置文件中配置
 * 4、相比较于filter，Interceptor可以获取spring请求的具体信息（如请求的controller，请求的方法名）
 * 5、拦截器会拦截所有的controller请求，包括spring框架的controller
 * @author MARS
 * @date 2018/7/17
 */
//@Component
public class TimerInterceptor implements HandlerInterceptor{
    /**
     * 请求调用前拦截
     * @param httpServletRequest
     * @param httpServletResponse
     * @param handler：是处理具体请求,其类型是 HandlerMethod
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest,
                             HttpServletResponse httpServletResponse,
                             Object handler) throws Exception {
        System.out.println("preHandle");
        // 获取handler处理controller的name
        System.out.println(((HandlerMethod)handler).getBean().getClass().getName());
        // 获取handler处理的方法名称
        System.out.println(((HandlerMethod)handler).getMethod().getName());
        httpServletRequest.setAttribute("startTime",System.currentTimeMillis());
        return true;
    }

    /**
     * 控制器方法调用之后 该方法生效
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle");
        Long start = (Long)httpServletRequest.getAttribute("startTime");
        System.out.println("time interceptor 耗时："+(System.currentTimeMillis() - start));
    }

    /**
     * 不管控制器是否抛出异常还是正确执行，均执行该方法
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o
     * @param e：失败时 e  中有异常信息，成功时 e 内容为空
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        System.out.println("afterCompletion");
        Long start = (Long)httpServletRequest.getAttribute("startTime");
        System.out.println("time interceptor 耗时："+(System.currentTimeMillis() - start));
        System.out.println("e is :" + e);
        // 该方法中的异常参数e，如果业务代码中抛出的是已经在全局异常中处理的自定义异常，该异常参数就会为null，
        // 如果抛出的异常类型没有定义在全局异常的拦截类型中，则该异常参数有值
    }
}
