package com.mars.security.core.social.qq.api;

/**
 * 服务提供商的 API 接口： 获取用户信息
 *
 * 第三方用户登录
 * QQ 接口
 * @Author: Mars
 * @Date: 2018/8/27 17:22
 */
public interface QQ {

    // 获取qq用户信息
    QQUserInfo getUserInfo();
}
