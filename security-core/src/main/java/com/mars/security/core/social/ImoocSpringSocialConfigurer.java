package com.mars.security.core.social;

import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * 继承SpringSocialConfigurer，实现自定义的配置
 * @Author: Mars
 * @Date: 2018/9/11 11:26
 */
public class ImoocSpringSocialConfigurer extends SpringSocialConfigurer{

    private String filterProcessesUrl ;

    public ImoocSpringSocialConfigurer(String filterProcessesUrl){
        this.filterProcessesUrl = filterProcessesUrl;
    }
    /**
     *
     * @param object：放置在过滤器链身上的filter
     * @param <T>
     * @return
     */
    @Override
    protected <T> T postProcess(T object) {
        // 设置自定义的url
        SocialAuthenticationFilter filter = (SocialAuthenticationFilter)super.postProcess(object);
        filter.setFilterProcessesUrl(filterProcessesUrl);
        return (T)filter;
    }
}
