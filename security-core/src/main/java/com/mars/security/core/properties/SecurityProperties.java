package com.mars.security.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自定义配置的总管理类
 * @author MARS
 * @date 2018/7/22
 */
@ConfigurationProperties(prefix = "mars.security")
public class SecurityProperties {

    private BrowserProperties browser = new BrowserProperties();

    /**
     *
     * 放入验证码类
     *
     */
    private ValidateCodeProperties code = new ValidateCodeProperties();

    /**
     * 放入社交properties类
     * @return
     */
    private SocialProperties social = new SocialProperties();

    public BrowserProperties getBrowser() {
        return browser;
    }

    public void setBrowser(BrowserProperties browser) {
        this.browser = browser;
    }

    public ValidateCodeProperties getCode() {
        return code;
    }

    public void setCode(ValidateCodeProperties code) {
        this.code = code;
    }

    public SocialProperties getSocial() {
        return social;
    }

    public void setSocial(SocialProperties social) {
        this.social = social;
    }
}
