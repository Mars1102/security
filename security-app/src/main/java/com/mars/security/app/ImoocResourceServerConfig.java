package com.mars.security.app;

import com.mars.security.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import com.mars.security.core.properties.SecurityConstants;
import com.mars.security.core.properties.SecurityProperties;
import com.mars.security.core.validate.code.ValidateCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * @author masisi
 * @date 2023/2/4 11:32
 * @Description 资源服务器
 */
@Configuration
@EnableResourceServer
public class ImoocResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Autowired
    protected AuthenticationSuccessHandler imoocAuthenticationSuccessHandler;
    @Autowired
    protected AuthenticationFailureHandler imoocAuthenticationFailureHandler;
    @Autowired
    private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;
    @Autowired
    private ValidateCodeSecurityConfig validateCodeSecurityConfig;
    @Autowired
    private SpringSocialConfigurer imoocSocialSecurityConfig;
    @Autowired
    private SecurityProperties securityProperties;
    // 安全配置处理
    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.formLogin()
                // 个性化登陆页面配置
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
                // 配置自定义处理登录url
                .loginProcessingUrl(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM)
                // 自定义配置成功登录后的处理
                .successHandler(imoocAuthenticationSuccessHandler)
                // 自定义配置登陆失败后的处理
                .failureHandler(imoocAuthenticationFailureHandler);

        http.apply(validateCodeSecurityConfig) // 添加自定义的过滤器，放置在 UsernamePasswordAuthenticationFilter 之前使用
                .and()
             .apply(smsCodeAuthenticationSecurityConfig) // 添加短信验证码过滤器
                .and()

             .apply(imoocSocialSecurityConfig) // 添加social过滤器
                .and()
             .authorizeRequests() // 开启请求做授权
                //.antMatchers("/authentication/require").permitAll() // 访问这个路径的使用不需要权限认证
                //访问这个路径的使用不需要权限认证
                .antMatchers(
                        SecurityConstants.DEFAULT_UNAUTHENTICATION_URL,
                        SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_MOBILE,
                        securityProperties.getBrowser().getLoginPage(),
                        SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX+"/*",
                        securityProperties.getBrowser().getSignUpUrl(),
                        "/user/regist"
                ).permitAll()
                // 对于任何请求
                .anyRequest()
                // 都需要做认证
                .authenticated()
                .and()
                // 跨站请求伪造防护功能关闭
                .csrf().disable();
    }
}
