package com.mars.service.impl;

import com.mars.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * @author MARS
 * @date 2018/7/17
 */

@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String greeting(String name) {
        System.out.println("greeting");
        return "Hello"+name;
    }
}
