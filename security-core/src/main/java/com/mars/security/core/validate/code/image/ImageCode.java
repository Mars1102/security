package com.mars.security.core.validate.code.image;

import com.mars.security.core.validate.code.ValidateCode;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * 图片验证码一般包含三个信息：
 * 1、图片
 * 2、随机数，存放在session中
 * 3、时间，过期时间
 * @author MARS
 * @date 2018/7/25
 */
public class ImageCode extends ValidateCode {

    private BufferedImage image;

    /**
     * 过期时间点
     * @param image
     * @param code
     * @param expireIn
     */
    public ImageCode(BufferedImage image, String code, int expireIn) {
        super(code,expireIn);
        this.image = image;
    }

    public ImageCode(BufferedImage image, String code, LocalDateTime expireTime) {
        super(code,expireTime);
        this.image = image;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

}
